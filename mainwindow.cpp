#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->choose->resize(QSize(190,50));
    ui->choose->move(40,20);
    ui->back->move(550,400);
    ui->back->hide();
    ui->convert->move(450,400);
    ui->convert->hide();
    ui->choose_second_1->resize(QSize(190,50));
    ui->choose_second_1->move(60,20);
    ui->choose_second_1->hide();
    ui->choose_second_2->resize(QSize(190,50));
    ui->choose_second_2->move(360,20);
    ui->choose_second_2->hide();
    ui->input->resize(QSize(250,40));
    ui->input->move(50,190);
    ui->input->hide();
    ui->output->resize(QSize(250,180));
    ui->output->move(350,120);
    ui->output->hide();
    connect(ui->choose,SIGNAL(currentIndexChanged(int)),this,SLOT(choose_change()));
    connect(ui->back,SIGNAL(clicked(bool)),this,SLOT(back()));
    connect(ui->convert,SIGNAL(clicked(bool)),this,SLOT(calculate()));
    length[0]=1;
    length[1]=3.2808;
    length[2]=39.37;
    length[3]=0.001;
    length[4]=100;
    length[5]=1000;
    length[6]=pow(10,9);
    length[7]=1.0936;
    length[8]=0.00062137;
    length[9]=1.057*pow(10,-16);
}

void MainWindow::choose_change(){
    if(ui->choose->currentText() != "Choose Quantity Name"){
        ui->choose->hide();
        ui->back->show();
        ui->convert->show();
        ui->choose_second_1->show();
        ui->choose_second_2->show();
        ui->input->show();
        ui->output->show();
        if(ui->choose->currentText() == "Length"){
            ui->choose_second_1->addItem(QWidget::tr("m"));
            ui->choose_second_1->addItem(QWidget::tr("ft"));
            ui->choose_second_1->addItem(QWidget::tr("in"));
            ui->choose_second_1->addItem(QWidget::tr("km"));
            ui->choose_second_1->addItem(QWidget::tr("cm"));
            ui->choose_second_1->addItem(QWidget::tr("mm"));
            ui->choose_second_1->addItem(QWidget::tr("nm"));
            ui->choose_second_1->addItem(QWidget::tr("yd"));
            ui->choose_second_1->addItem(QWidget::tr("mile"));
            ui->choose_second_1->addItem(QWidget::tr("light year"));
            ui->choose_second_2->addItem(QWidget::tr("m"));
            ui->choose_second_2->addItem(QWidget::tr("ft"));
            ui->choose_second_2->addItem(QWidget::tr("in"));
            ui->choose_second_2->addItem(QWidget::tr("km"));
            ui->choose_second_2->addItem(QWidget::tr("cm"));
            ui->choose_second_2->addItem(QWidget::tr("mm"));
            ui->choose_second_2->addItem(QWidget::tr("nm"));
            ui->choose_second_2->addItem(QWidget::tr("yd"));
            ui->choose_second_2->addItem(QWidget::tr("mile"));
            ui->choose_second_2->addItem(QWidget::tr("light year"));
        }
        if(ui->choose->currentText() == "Thermodynamic Temperature"){
            ui->choose_second_1->addItem(QWidget::tr("Celsius"));
            ui->choose_second_1->addItem(QWidget::tr("Kelvin"));
            ui->choose_second_1->addItem(QWidget::tr("Fahrenheit"));
            ui->choose_second_2->addItem(QWidget::tr("Celsius"));
            ui->choose_second_2->addItem(QWidget::tr("Kelvin"));
            ui->choose_second_2->addItem(QWidget::tr("Fahrenheit"));
        }
    }
}

void MainWindow::back(){
    int rm=0;
    if(ui->choose->currentText()=="Length")rm=9;
    if(ui->choose->currentText()=="Thermodynamic Temperature")rm=2;
    while(rm>=0){
        ui->choose_second_1->removeItem(rm);
        ui->choose_second_2->removeItem(rm);
        rm--;
    }
    ui->choose->setCurrentIndex(0);
    ui->choose->show();
    ui->back->hide();
    ui->convert->hide();
    ui->choose_second_1->hide();
    ui->choose_second_2->hide();
    ui->input->hide();
    ui->output->hide();
}

void MainWindow::calculate(){
    if(ui->choose->currentText() == "Length"){
        double num_i=ui->input->text().toDouble();
        double num_f=num_i*length[ui->choose_second_2->currentIndex()]/length[ui->choose_second_1->currentIndex()];
        QString str=QString::number(num_f);
        ui->output->setText(str);
    }
    if(ui->choose->currentText() == "Thermodynamic Temperature"){
        if(ui->choose_second_1->currentIndex() == ui->choose_second_2->currentIndex()){
            ui->output->setText(ui->input->text());
        }
        if(ui->choose_second_1->currentIndex() == 0 && ui->choose_second_2->currentIndex() == 1){
            double num_i=ui->input->text().toDouble();
            double num_f=num_i+273.15;
            QString str=QString::number(num_f);
            ui->output->setText(str);
        }
        if(ui->choose_second_1->currentIndex() == 0 && ui->choose_second_2->currentIndex() == 2){
            double num_i=ui->input->text().toDouble();
            double num_f=(num_i*9/5)+32;
            QString str=QString::number(num_f);
            ui->output->setText(str);
        }
        if(ui->choose_second_1->currentIndex() == 1 && ui->choose_second_2->currentIndex() == 0){
            double num_i=ui->input->text().toDouble();
            double num_f=num_i-273.15;
            QString str=QString::number(num_f);
            ui->output->setText(str);
        }
        if(ui->choose_second_1->currentIndex() == 1 && ui->choose_second_2->currentIndex() == 2){
            double num_i=ui->input->text().toDouble();
            double num_f=((num_i-273.15)*9/5)+32;
            QString str=QString::number(num_f);
            ui->output->setText(str);
        }
        if(ui->choose_second_1->currentIndex() == 2 && ui->choose_second_2->currentIndex() == 0){
            double num_i=ui->input->text().toDouble();
            double num_f=(num_i-32)*5/9;
            QString str=QString::number(num_f);
            ui->output->setText(str);
        }
        if(ui->choose_second_1->currentIndex() == 2 && ui->choose_second_2->currentIndex() == 1){
            double num_i=ui->input->text().toDouble();
            double num_f=((num_i-32)*5/9)+273.15;
            QString str=QString::number(num_f);
            ui->output->setText(str);
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
