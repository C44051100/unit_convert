#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <math.h>
#include <QWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void choose_change();
    void back();
    void calculate();

private:
    Ui::MainWindow *ui;
    double length[10];
    double temperature[3];
};

#endif // MAINWINDOW_H
